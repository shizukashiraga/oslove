local processes = {}

function love.load()
    love.mouse.setVisible(false)
    love.window.setMode(1280, 720, {
        fullscreen = false,
        fullscreentype = "desktop",
        vsync = false,
        resizable = true,
        borderless = false
    })
    --love.graphics.setBackgroundColor(221/255, 42/255, 98/255, 1)
    love.graphics.setBackgroundColor(0.5, 0.75, 1, 1)

    table.insert(processes, require("drive.packages.test2.main"))
    processes[table.maxn(processes)].load()

    table.insert(processes, require("drive.packages.test.main"))
    processes[table.maxn(processes)].load()
end

function love.update(dt)
    for i = 1, #processes, 1 do
        processes[i].update(dt)
    end
end

local cursor = love.graphics.newImage("drive/system/cursor/normal.png")

function love.draw()
    local mouseX = love.mouse.getX()
    local mouseY = love.mouse.getY()

    for i = 1, #processes, 1 do
        processes[i].draw()
    end

    -- draw cursor
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(cursor, mouseX, mouseY)

    love.graphics.print(tostring(love.timer.getFPS( )), 1, 1)
end

function love.mousepressed(x, y, button, istouch)
    local mouseX = love.mouse.getX()
    local mouseY = love.mouse.getY()

    if button == 1 then
        local nextFocus = 0

        for i = 1, table.maxn(processes), 1 do
            if mouseX >= processes[i].windowX and mouseX <= processes[i].windowX+mouseX and mouseY >= processes[i].windowY and mouseY <= processes[i].windowY+mouseY  then
                nextFocus = i
            end
        end

        if nextFocus == 0 then else
            print("---------------------------")
            print("nextFocus = "..tostring(nextFocus))
            print("---------------------------")

            local temp = processes[#processes]
            processes[#processes] = processes[nextFocus]
            processes[nextFocus] = temp
        end
    end
end
function love.conf(t)
    t.identity = "org.Azapru.OSLove"

    t.window.title = "OSLove"

    t.modules.joystick = false
    t.modules.physics = false
end
local oslove = {}

local function drawCenteredText(rectX, rectY, rectWidth, rectHeight, text)
	local font       = love.graphics.getFont()
	local textWidth  = font:getWidth(text)
	local textHeight = font:getHeight()
	love.graphics.print(text, rectX+rectWidth/2, rectY+rectHeight/2, 0, 1, 1, textWidth/2, textHeight/2)
end

local globalWindowX = 0
local globalWindowY = 0
local globalWindowWidth = 0
local globalWindowHeight = 0

------------------------------------------------------------ Objects ------------------------------------------------------------

local buttons = {}
local nextButtonID = 1
local lists = {}
local nextListID = 1

------------------------------------------------------------ Load ------------------------------------------------------------

function oslove.load(windowX, windowY, windowWidth, windowHeight)
    local major, minor, revision, codename = love.getVersion()
    oslove.log(0, "Running with ".._VERSION..", LÖVE "..tostring(major).."."..tostring(minor).."."..tostring(revision).." "..tostring(codename))

    -- LOVE version check

    local expectedVer = 11

    if major == expectedVer then else
        oslove.log(1, "This package is running with an unsupported version of LOVE!")
        oslove.log(1, "This package uses OSLOVE module made for LOVE "..expectedVer..", and you are running this package with LOVE "..major)
        if major > expectedVer then
            oslove.log(1, "The OSLOVE module of this package needs to be updated! (or maybe OSLOVE just hasn't been yet updated to support this version of LOVE?)")
        end
        if major < expectedVer then
            oslove.log(1, "Please update your system's LOVE runtime!")
        end
    end
end

------------------------------------------------------------ Update ------------------------------------------------------------

function oslove.update(dt, windowX, windowY, windowWidth, windowHeight)

    globalWindowX = windowX
    globalWindowY = windowY
    globalWindowWidth = windowWidth
    globalWindowHeight = windowHeight
    
    -- Get mouse coordinates

    local mouseX = love.mouse.getX() - windowX
    local mouseY = love.mouse.getY() - windowY

    -- Check if mouse is inside window
    
    local mouseInside = false
    if love.mouse.getX() >= windowX and love.mouse.getX() <= windowX+windowWidth and love.mouse.getY() >= windowY and love.mouse.getY() <= windowY+windowHeight then
        mouseInside = true
    else
        mouseInside = false
    end


        -- Update buttons
        
    for i = 1, table.maxn(buttons), 1 do
        if buttons[i] == nil then else
            local x = buttons[i].x
            local y = buttons[i].y
            local height = buttons[i].height
            local width = buttons[i].width

            if mouseX >= x and mouseX <= x+width and mouseY >= y and mouseY <= y+height and mouseInside then
                buttons[i].conditions.mouseOver = true
            else
                buttons[i].conditions.mouseOver = false
            end
        end
    end

    -- Update lists

    for i = 1, table.maxn(lists), 1 do
        if lists[i] == nil then else
            local x = lists[i].x
            local y = lists[i].y
            local height = lists[i].height
            local width = lists[i].width

            if mouseX >= x and mouseX <= x+width and mouseY >= y and mouseY <= y+height and love.mouse.isDown(1) and mouseInside then
                lists[i].selection = (math.floor(((mouseY-y)/lists[i].spacing)+1))-lists[i].scroll
                if lists[i].selection > #lists[i].content then
                    lists[i].selection = #lists[i].content
                end
            end
        end
    end
end

------------------------------------------------------------ Draw ------------------------------------------------------------

function oslove.drawBg(windowX, windowY, windowWidth, windowHeight)
    StencilShape = function()
        love.graphics.rectangle("fill", windowX, windowY, windowWidth, windowHeight)
    end
    love.graphics.stencil(StencilShape, "replace", 1)
    love.graphics.setStencilTest("greater", 0)
    
    --love.graphics.setColor(0.825, 0.825, 0.825, 1)
    love.graphics.setColor(0.45, 0.45, 0.46, 1)
    love.graphics.rectangle("fill", windowX, windowY, windowWidth, windowHeight)
end

function oslove.draw(windowX, windowY, windowWidth, windowHeight)

    -- Drawing buttons

    for i = 1, table.maxn(buttons), 1 do
        if buttons[i] == nil then elseif oslove.button.isHeld(i) then -- Held
            local x = buttons[i].x + windowX
            local y = buttons[i].y + windowY
            local height = buttons[i].height
            local width = buttons[i].width
            local text = buttons[i].text
            -- Dark lines
            love.graphics.setColor(0.5, 0.5, 0.5, 1)
            love.graphics.rectangle("fill", x, y+height-2, width, 2)
            love.graphics.rectangle("fill", x+width-2, y, 2, height)
            -- Light lines
            love.graphics.setColor(0.25, 0.25, 0.25, 1)
            love.graphics.rectangle("fill", x, y, width, 2)
            love.graphics.rectangle("fill", x, y, 2, height)
            -- Fill
            love.graphics.setColor(0.8, 0.8, 0.8, 1)
            love.graphics.rectangle("fill", x+2, y+2, width-4, height-4)
            love.graphics.setColor(0.82, 0.82, 0.82, 1)
            love.graphics.rectangle("fill", x+2, y+2, width-4, (height-4)/2)
            -- Text
            love.graphics.setColor(0, 0, 0, 1)
            drawCenteredText(x+1, y+1, width, height, text)
        else -- Unheld
            local x = buttons[i].x + windowX
            local y = buttons[i].y + windowY
            local height = buttons[i].height
            local width = buttons[i].width
            local text = buttons[i].text
            -- Dark lines
            love.graphics.setColor(0.25, 0.25, 0.25, 1)
            love.graphics.rectangle("fill", x, y+height-2, width, 2)
            love.graphics.rectangle("fill", x+width-2, y, 2, height)
            -- Light lines
            love.graphics.setColor(0.5, 0.5, 0.5, 1)
            love.graphics.rectangle("fill", x, y, width, 2)
            love.graphics.rectangle("fill", x, y, 2, height)
            -- Fill
            if buttons[i].conditions.mouseOver == true then -- Checking if mouse is over a button, to make a button lighter
                love.graphics.setColor(0.95, 0.95, 0.95, 1)
                love.graphics.rectangle("fill", x+2, y+2, width-4, height-4)
                love.graphics.setColor(0.97, 0.97, 0.97, 1)
                love.graphics.rectangle("fill", x+2, y+2, width-4, (height-4)/2)
            else
                love.graphics.setColor(0.89, 0.89, 0.89, 1)
                love.graphics.rectangle("fill", x+2, y+2, width-4, height-4)
                love.graphics.setColor(0.9, 0.9, 0.9, 1)
                love.graphics.rectangle("fill", x+2, y+2, width-4, (height-4)/2)
            end
            -- Text
            love.graphics.setColor(0, 0, 0, 1)
            drawCenteredText(x, y, width, height, text)
        end
    end

    -- Drawing lists

    for i = 1, table.maxn(lists), 1 do
        if lists[i] == nil then else
            local x = lists[i].x + windowX
            local y = lists[i].y + windowY
            local height = lists[i].height
            local width = lists[i].width

            -- Background
            love.graphics.setColor(1, 1, 1, 1)
            love.graphics.rectangle("fill", x, y, width, height)
            
            -- Selection
            if lists[i].selection > 0 and lists[i].selection+lists[i].scroll > 0 and lists[i].selection+lists[i].scroll < height/lists[i].spacing then
                love.graphics.setColor(0.65, 0.75, 0.90, 1)
                love.graphics.rectangle("fill", x, y+(lists[i].selection+lists[i].scroll-1)*lists[i].spacing, width, lists[i].spacing)
            end

            -- Items/Text
            for content = 1, table.maxn(lists[i].content), 1 do
                if (content+lists[i].scroll)*lists[i].spacing <= height and (content+lists[i].scroll-1)*lists[i].spacing >= 0 then
                    love.graphics.setColor(0, 0, 0, 1)
                    love.graphics.print(lists[i].content[content], x, y+(lists[i].spacing*(content+lists[i].scroll-1))+(lists[i].spacing/2-8))
                end
            end

            -- Border
            love.graphics.setColor(0, 0, 0, 1)
            love.graphics.rectangle("line", x-1, y-1, width+1, height+1)
        end
    end

    love.graphics.setStencilTest()

    -- Drawing window
    love.graphics.setColor(0, 0, 0, 1)

    love.graphics.rectangle("fill", windowX-1, windowY-1, windowWidth+2, 1)
    love.graphics.rectangle("fill", windowX-1, windowY+windowHeight, windowWidth+2, 1)
    love.graphics.rectangle("fill", windowX-1, windowY-1, 1, windowHeight+2)
    love.graphics.rectangle("fill", windowX+windowWidth, windowY-1, 1, windowHeight+2)

    love.graphics.rectangle("fill", windowX-5, windowY-5-30, windowWidth+10, 1)
    love.graphics.rectangle("fill", windowX-5, windowY+windowHeight+4, windowWidth+10, 1)
    love.graphics.rectangle("fill", windowX-5, windowY-5-30, 1, windowHeight+10+30)
    love.graphics.rectangle("fill", windowX+windowWidth+4, windowY-5-30, 1, windowHeight+10+30)

    love.graphics.setColor(0.55, 0.55, 0.56, 1)

    love.graphics.rectangle("fill", windowX-4, windowY-4-30, windowWidth+8, 3+30)
    love.graphics.rectangle("fill", windowX-4, windowY+windowHeight+1, windowWidth+8, 3)
    love.graphics.rectangle("fill", windowX-4, windowY-4, 3, windowHeight+8)
    love.graphics.rectangle("fill", windowX+windowWidth+1, windowY-4, 3, windowHeight+8)

end

------------------------------------------------------------ Buttons ------------------------------------------------------------

oslove.button = {}

function oslove.button.create(x, y, width, height, text, onClickFunction)
    buttons[nextButtonID] = {x=x, y=y, width=width, height=height, text=text, onClickFunction=onClickFunction, conditions={mouseOver=false}}
    nextButtonID = nextButtonID + 1
    oslove.log(0, "Created button object at x="..tostring(x).." y="..tostring(y).." with ID "..tostring(nextButtonID-1))
    return nextButtonID - 1
end

function oslove.button.isMouseOver(id)
    return buttons[id].conditions.mouseOver
end

function oslove.button.isHeld(id)
    if love.mouse.isDown(1) and oslove.button.isMouseOver(id) then
        return true
    else
        return false
    end
end

-- Check if a button has been clicked
function love.mousereleased()
    if love.mouse.getX() >= globalWindowX and love.mouse.getX() <= globalWindowX+globalWindowWidth and love.mouse.getY() >= globalWindowY and love.mouse.getY() <= globalWindowY+globalWindowHeight then
        for i = 1, table.maxn(buttons), 1 do
            if buttons[i] == nil then else
                local mouseX = love.mouse.getX()
                local mouseY = love.mouse.getY()

                local x = buttons[i].x + globalWindowX
                local y = buttons[i].y + globalWindowY
                local height = buttons[i].height
                local width = buttons[i].width

                if mouseX >= x and mouseX <= x+width and mouseY >= y and mouseY <= y+height then
                    buttons[i].onClickFunction()
                end
            end
        end
    end
end

------------------------------------------------------------ Lists ------------------------------------------------------------

oslove.list = {}

function oslove.list.create(x, y, width, height)
    lists[nextListID] = {x=x, y=y, width=width, height=height, content={}, selection=0, spacing=16, scroll=0}
    nextListID = nextListID + 1
    oslove.log(0, "Created list object at x="..tostring(x).." y="..tostring(y).." with ID "..tostring(nextListID-1))
    return nextListID - 1
end

function oslove.list.setSpacing(id, spacing)
    lists[id].spacing = spacing
end

function oslove.list.add(id, text)
    table.insert(lists[id].content, text)
end

function oslove.list.getSelection(id)
    return lists[id].selection
end

function oslove.list.getItem(id, selection)
    return lists[id].content[selection]
end

function love.wheelmoved(scrollx, scrolly)
    if love.mouse.getX() >= globalWindowX and love.mouse.getX() <= globalWindowX+globalWindowWidth and love.mouse.getY() >= globalWindowY and love.mouse.getY() <= globalWindowY+globalWindowHeight then
        for i = 1, table.maxn(lists), 1 do
            if lists[i] == nil then else
                local mouseX = love.mouse.getX()
                local mouseY = love.mouse.getY()
                local x = lists[i].x + globalWindowX
                local y = lists[i].y + globalWindowY
                local height = lists[i].height
                local width = lists[i].width

                if mouseX >= x and mouseX <= x+width and mouseY >= y and mouseY <= y+height then
                    if scrolly > 0 then
                        lists[i].scroll = lists[i].scroll + 1
                    elseif scrolly < 0 and lists[i].scroll-(height/lists[i].spacing) > -#lists[i].content then
                        lists[i].scroll = lists[i].scroll - 1
                    end

                    if lists[i].scroll > 0 then
                        lists[i].scroll = 0
                    end
                end
            end
        end
    end
end

------------------------------------------------------------ Log ------------------------------------------------------------

function oslove.log(level, message)
    local levels = {"\27[32m[INFO]\27[37m ","\27[33m[WARNING] ","\27[31m[ERROR] "}
    print(tostring(levels[level+1])..tostring(message))
end


return oslove
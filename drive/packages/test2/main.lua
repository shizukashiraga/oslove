local app = {}
local oslove = require "drive.packages.test2.oslove"
app.windowHeight = 640
app.windowWidth = 480
app.windowX = 32
app.windowY = 96

function app.load() oslove.load(app.windowX, app.windowY, app.windowHeight, app.windowWidth)
    List = oslove.list.create(-64, 128, 400, 100)
    oslove.list.setSpacing(List, 16)

    Button1 = oslove.button.create(-32, -16, 64, 32, "Balls", Dummy)
    Button2 = oslove.button.create(128, 64, 64, 32, "Balls", Dummy)

    oslove.list.add(List, "Hi chat")
    oslove.list.add(List, "balls balls")
    oslove.list.add(List, "kuru kuru kuru kuru")
    oslove.list.add(List, "what color is your bugatti?")
    oslove.list.add(List, "Lorem Ipsum i forgor")
    oslove.list.add(List, "Hi chat")
    oslove.list.add(List, "balls balls")
    oslove.list.add(List, "kuru kuru kuru kuru")
    oslove.list.add(List, "what color is your bugatti?")
    oslove.list.add(List, "Lorem Ipsum i forgor")
end

function app.update(dt) oslove.update(dt, app.windowX, app.windowY, app.windowHeight, app.windowWidth)
    
end

function app.draw()
    oslove.drawBg(app.windowX, app.windowY, app.windowHeight, app.windowWidth)

    oslove.draw(app.windowX, app.windowY, app.windowHeight, app.windowWidth)
end

function Dummy() end

return app